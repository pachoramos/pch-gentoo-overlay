# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# Version provided by topspin-4.2 installer

EAPI=8
inherit readme.gentoo-r1 rpm udev xdg

DESCRIPTION="Software protection and license management by WIBU"
HOMEPAGE="https://www.wibu.com/products/codemeter.html"
SRC_URI="CodeMeter-7.40.5001.rpm"

LICENSE="codemeter"
SLOT="0"
KEYWORDS="~amd64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

RESTRICT="fetch"

# acct-user/codemeter
# acct-group/codemeter
RDEPEND="
	>=media-libs/fontconfig-2.11:1.0
	>=media-libs/freetype-2.6:2
	>=sys-apps/lsb-release-3
	sys-libs/zlib
	sys-process/procps
	x11-libs/libICE
	x11-libs/libSM
	x11-libs/libxcb[xkb]
	x11-libs/libXext
	x11-libs/libxkbcommon
	x11-libs/xcb-util-wm
	x11-libs/xcb-util
	x11-libs/xcb-util-image
	x11-libs/xcb-util-keysyms
	x11-libs/xcb-util-renderutil
	virtual/libusb:1
	virtual/udev
"

QA_PREBUILT="*"
S="${WORKDIR}"

DOC_CONTENTS="In order to manage licenses, add your user to 'codemeter' group."

PATCHES=(
	# With this version daemon is stopped sometimes, keep it alive
	"${FILESDIR}"/${P}-keep-alive.patch
)

pkg_nofetch() {
	elog "Please download ${SRC_URI} from"
	elog "https://www.wibu.com/support/user/user-software.html"
	elog "and place it into your DISTDIR directory."
}

src_install() {
	insinto /
	doins -r etc lib usr

	# Fix permissions
	chmod +x "${ED}"/usr/bin/* || die
	chmod +x "${ED}"/usr/sbin/* || die
	readme.gentoo_create_doc
}

pkg_postinst() {
	# Cruft needed to have a working codemeter (otherwise it fails when
	# trying to install a license

	# create CodeMeter & WebAdmin log, CmCloud, and NamedUser dirs
	mkdir -p /var/log/CodeMeter
	mkdir -p /var/log/CodeMeter/WebAdmin
	mkdir -p /var/lib/CodeMeter/CmAct/
	mkdir -p /var/lib/CodeMeter/Backup
	mkdir -p /var/lib/CodeMeter/CmCloud
	mkdir -p /var/lib/CodeMeter/NamedUser

	# change ownership to 'daemon:daemon' (user:group)
	chown -R daemon:daemon /etc/wibu/CodeMeter
	chown -R daemon:daemon /etc/wibu/CodeMeter/Server.ini
	chown -R daemon:daemon /var/log/CodeMeter
	chown -R daemon:daemon /var/lib/CodeMeter

	/usr/sbin/CodeMeterLin -x

	mkdir -p /var/lib/CodeMeter/NamedUser
	mkdir -p /var/log/CodeMeter
	chmod a+rwx /var/log/CodeMeter
	chmod a+rwx /var/lib/CodeMeter/Backup
	chmod a+rwx /var/lib/CodeMeter/CmAct
	chmod a+rwx /var/lib/CodeMeter/CmCloud
	chmod a+rwx /var/lib/CodeMeter/NamedUser

	mkdir -p -m755 /var/log/CodeMeter/WebAdmin
	chown daemon:daemon /var/log/CodeMeter/WebAdmin

	udev_reload
	systemctl daemon-reload
	xdg_pkg_postinst
	readme.gentoo_print_elog
}

pkg_postrm() {
	udev_reload
	xdg_pkg_postrm
}
